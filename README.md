# Youtube Moving Sharing Web App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Getting Started
- [How to run this project?](#how-to-run-this-project)
- [Results and Test Cases](#results-and-tests)
- [Live URL](#live-url)

## How to run this project?

### 1. Clone repository:
```bash
>>> git clone https://gitlab.com/movies-sharing/movies-sharing-web.git
>>> cd movies-sharing-web
```

### 2. Install dependencies:
```bash
>>> yarn install
```

### 3. Create .env file from .env_template:
```bash
>>> cp .env_template .env
```

Note: You need to run the [backend](https://gitlab.com/movies-sharing/movies-sharing-api) first.
- From URL: https://gitlab.com/movies-sharing/movies-sharing-api
- Here I use port 4000 for backend

### 4. Run project:
```bash
>>> yarn start
```

## Results and Tests
1. In Home page, you can see the list of movies that are shared by other users.
   
   ![test](results/Homepage.png)


2. To share a movie, you need to login first. If you don't have an account, you can register a new one.
    
    ![test](results/Homepage-after-login.png)
3. After login, you can share a movie by clicking on the "Share" button.
        
![test](results/share.png)

4. After sharing a movie successfully, a notification will be shown.
   ![test](results/share-success.png)


### Testing
- [Jest](https://jestjs.io/) are used for testing.
```bash
>>> yarn test
```
After running the command above, you can see the test results.
![test](results/testing.png)

## Live URL
- http://159.203.160.245/



