import React, {createContext, ReactNode, useEffect, useReducer} from 'react';
import {ActionMap, AuthState, AuthUserName, JWTContextType} from "../@types/auth";
import {User} from "../@types/user";
import {setSession} from "../utils/jwt";
import axiosGatewayInstance from "../utils/axios";
// utils


// ----------------------------------------------------------------------

enum Types {
  Initial = 'INITIALIZE',
  Login = 'LOGIN',
  Logout = 'LOGOUT',
  Register = 'REGISTER',
}

type JWTAuthPayload = {
  [Types.Initial]: {
    isAuthenticated: boolean;
    username: AuthUserName;
  };
  [Types.Login]: {
    username: AuthUserName;
  };
  [Types.Logout]: undefined;
  [Types.Register]: {
    username: AuthUserName;
  };
};

export type JWTActions = ActionMap<JWTAuthPayload>[keyof ActionMap<JWTAuthPayload>];

const initialState: AuthState = {
  isAuthenticated: false,
  isInitialized: false,
  username: null,
};

const JWTReducer = (state: AuthState, action: JWTActions) => {
  switch (action.type) {
    case 'INITIALIZE':
      return {
        isAuthenticated: action.payload.isAuthenticated,
        isInitialized: true,
        username: action.payload.username,
      };
    case 'LOGIN':
      return {
        ...state,
        isAuthenticated: true,
        username: action.payload.username,
      };
    case 'LOGOUT':
      return {
        ...state,
        isAuthenticated: false,
        username: null,
      };

    case 'REGISTER':
      return {
        ...state,
        isAuthenticated: true,
        username: action.payload.username,
      };

    default:
      return state;
  }
};

const AuthContext = createContext<JWTContextType | null>(null);

// ----------------------------------------------------------------------

type AuthProviderProps = {
  children: ReactNode;
};

function AuthProvider({ children }: AuthProviderProps) {
  const [state, dispatch] = useReducer(JWTReducer, initialState);

  useEffect(() => {
    const initialize = async () => {
      try {
        const accessToken = window.localStorage.getItem('accessToken');
        const localUserName = localStorage.getItem('username') || '';
        const userData: User = {
          username: localUserName,
          password: '',
        }
        if (accessToken) {
          setSession(accessToken, false);
          await axiosGatewayInstance.post('/api/user/token/verify');
          dispatch({
            type: Types.Initial,
            payload: {
              isAuthenticated: true,
              username: userData.username,
            },
          });
        } else {
          dispatch({
            type: Types.Initial,
            payload: {
              isAuthenticated: false,
              username: null,
            },
          });
        }
      } catch (err) {
        console.error(err);
        dispatch({
          type: Types.Initial,
          payload: {
            isAuthenticated: false,
            username: null,
          },
        });
      }
    };

    initialize();
  }, []);
  

  const login = async (username: string, password: string) => {
    try {
      const response = await axiosGatewayInstance.post<User>('/api/user/login', {
        username,
        password,
      });
      const user = response.data;
      const token = response.headers.authorization;

      setSession(token, true, user.username);
      dispatch({
        type: Types.Login,
        payload: {
          username: user.username
        },
      });
    }
    catch (e) {
      throw new Error(e);
    }
    
  };

  const register = async (username: string, password: string) => {
    try {
      const response = await axiosGatewayInstance.post('/api/user/register', {
        username,
        password
      });
      const user = response.data;
      const token = response.headers.authorization;

      setSession(token, true, user.username);
      dispatch({
        type: Types.Register,
        payload: {
          username: user.username,
        },
      });
    }
    catch (e) {
      throw new Error(e);
    }
  };

  const logout = async () => {
    setSession(null, false);
    dispatch({ type: Types.Logout });
  };

  return (
    <AuthContext.Provider
      value={{
        ...state,
        method: 'jwt',
        login,
        logout,
        register,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
}

export { AuthContext, AuthProvider };
