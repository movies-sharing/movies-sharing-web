import { combineReducers } from 'redux';
import storage from 'redux-persist/lib/storage';
import movieReducer from './slices/movies';

// ----------------------------------------------------------------------

const rootPersistConfig = {
  key: 'root',
  storage,
  keyPrefix: 'redux-',
  whitelist: [],
};

const rootReducer = combineReducers({
  movies: movieReducer,
});

export { rootPersistConfig, rootReducer };
