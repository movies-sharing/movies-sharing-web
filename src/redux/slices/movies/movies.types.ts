import { ApiCallError } from "../../../@types/apiResponse";
import { SerializedError } from "@reduxjs/toolkit";
import {Movie} from "../../../@types/movie";

export type MovieState = {
    isLoading: boolean;
    error?: Error | ApiCallError | SerializedError;
    list: Movie[];
}