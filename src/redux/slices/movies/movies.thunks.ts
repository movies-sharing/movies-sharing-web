
import { createAsyncThunk } from '@reduxjs/toolkit';
import {Movie} from "../../../@types/movie";
import axiosGatewayInstance from "../../../utils/axios";
import {ApiCallError} from "../../../@types/apiResponse";

export const getMovies = createAsyncThunk<Movie[], undefined, { rejectValue: ApiCallError }>(
    'movies/list', async (_, { rejectWithValue }) => {
        try {
            const response = await axiosGatewayInstance.get(`/api/movies`);
            return response.data;
        } catch (err) {
            const error: ApiCallError = err;
            return rejectWithValue(error);
        }
    })
export const shareMovie = createAsyncThunk<Movie, string, { rejectValue: ApiCallError }>(
    'movies/share', async (youtubeUrl, { rejectWithValue }) => {
        try {
            const response = await axiosGatewayInstance.post(`/api/movies/share`, { youtubeUrl });
            return response.data;
        } catch (err) {
            const error: ApiCallError = err;
            return rejectWithValue(error);
        }
    })