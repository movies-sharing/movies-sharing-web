import {createSlice} from '@reduxjs/toolkit';
import { getMovies, shareMovie } from './movies.thunks';
import { MovieState } from './movies.types';




const initialState: MovieState = {
    isLoading: false,
    error: undefined,
    list: []
};


const slice = createSlice({
    name: 'movies',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(getMovies.pending, (state) => {
            state.isLoading = true;
        })
            .addCase(getMovies.fulfilled, (state, action) => {
                state.isLoading = false;
                state.list = action.payload;
            })
            .addCase(getMovies.rejected, (state, { payload, error }) => {
                state.isLoading = false;
                state.error = payload ?? error;
            })
            .addCase(shareMovie.pending, (state) => {
                state.isLoading = true;
            })
            .addCase(shareMovie.fulfilled, (state, action) => {
                state.isLoading = false;
                state.list = [action.payload, ...state.list];
            })
            .addCase(shareMovie.rejected, (state, { payload, error }) => {
                state.isLoading = false;
                state.error = payload ?? error;
            })
    }
});

// Reducer
export default slice.reducer;