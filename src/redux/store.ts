import {configureStore, PreloadedState} from '@reduxjs/toolkit';
import {
    useDispatch as useAppDispatch,
    useSelector as useAppSelector,
    TypedUseSelectorHook,
} from 'react-redux';
import { persistStore, persistReducer } from 'redux-persist';
import logger from 'redux-logger';
import { rootPersistConfig, rootReducer } from './rootReducer';
// ----------------------------------------------------------------------

const isProduction = process.env.NODE_ENV === 'production';

const store = (preloadedState?: PreloadedState<RootState>) => {
    return configureStore({
        reducer: persistReducer(rootPersistConfig, rootReducer),
        middleware: (getDefaultMiddleware) => {
            // disable logger in production
            if (isProduction) {
                return getDefaultMiddleware({
                    serializableCheck: false,
                    immutableCheck: false,
                });
            }
            return getDefaultMiddleware({
                serializableCheck: false,
                immutableCheck: false,
            }).concat(logger);
        },
        devTools: !isProduction,
        preloadedState
    });
}


const persistor = persistStore(store());

export type RootState = ReturnType<typeof rootReducer>;
export type AppStore = ReturnType<typeof store>;

export type AppDispatch = AppStore['dispatch'];

const { dispatch } = store();

const useDispatch = () => useAppDispatch<AppDispatch>();

const useSelector: TypedUseSelectorHook<RootState> = useAppSelector;

export { store, persistor, dispatch, useSelector, useDispatch };
