export type ApiResponse<T> = {
    data: T;
    code: any;
    message: string;
    stackTraces?: String;
}

export type ApiCallError = {
    details: any;
    code: any;
    message: string;
}