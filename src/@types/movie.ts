export type Movie = {
    id: number;
    title: string;
    description: string;
    sharedBy: string;
    youtubeUrl: string;
    videoId: string;
    createdAt: Date;
}