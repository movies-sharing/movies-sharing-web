// ----------------------------------------------------------------------

export type ActionMap<M extends { [index: string]: any }> = {
  [Key in keyof M]: M[Key] extends undefined
    ? {
        type: Key;
      }
    : {
        type: Key;
        payload: M[Key];
      };
};

export type AuthUserName = null | string;

export type AuthState = {
  isAuthenticated: boolean;
  isInitialized: boolean;
  username: AuthUserName;
};

export type JWTContextType = {
  isAuthenticated: boolean;
  isInitialized: boolean;
  username: AuthUserName;
  method: 'jwt';
  login: (username: string, password: string) => Promise<void>;
  register: (username: string, password: string) => Promise<void>;
  logout: () => Promise<void>;
};
