import { Suspense, lazy, ElementType } from 'react';
import {Navigate, useRoutes} from 'react-router-dom';
import DashboardLayout from "../layouts";
import AuthGuard from "../guards/AuthGuard";
import GuestGuard from "../guards/GuestGuard";
// layouts
// components

// ----------------------------------------------------------------------

const Loadable = (Component: ElementType) => function(props: any) {

  return (
    <Suspense>
      <Component {...props} />
    </Suspense>
  );
};

export default function Router() {
    return useRoutes([
        {
            path: '/',
            element: <DashboardLayout />,
            children: [
                {
                    path: '/',
                    element: (
                        <GuestGuard>
                            <Home />
                        </GuestGuard>
                    )
                },
                {
                    path: '/share',
                    element: (
                        <AuthGuard>
                            <Share />
                        </AuthGuard>
                    )
                }
            ]
        },
        {
            path: '*',
            element: <Navigate to="/" replace />,
        },
    ]);
}



const Home = Loadable(lazy(() => import('../pages/Home')));
const Share = Loadable(lazy(() => import('../pages/Share')));
