import * as Yup from 'yup';
import {User} from "../../@types/user";
import {FormProvider, RHFTextField} from "../../components/hook-form";
import {Stack} from "@mui/material";
import {LoadingButton} from "@mui/lab";
import {yupResolver} from "@hookform/resolvers/yup";
import {useForm} from "react-hook-form";
import useAuthContext from "../../hooks/useAuthContext";
import {useSnackbar} from "notistack";
import React from "react";


type FormValuesProps = User;

export default function LoginForm() {
    const { login } = useAuthContext();
    const { enqueueSnackbar } = useSnackbar();
    const LoginSchema = Yup.object().shape({
        username: Yup.string().required('Username is required'),
        password: Yup.string().required('Password is required'),
    });

    const defaultValues = {
        email: '',
        password: '',
    };

    const form = useForm<FormValuesProps>({
        defaultValues,
        resolver: yupResolver(LoginSchema),
    });

    const { handleSubmit, formState } = form;

    const onSubmit = async (data: FormValuesProps) => {
        try {
            await login(data.username, data.password);
            enqueueSnackbar('Login successful', { variant: 'success' });
        }
        catch (error) {
            enqueueSnackbar('Login failed', { variant: 'error' });
        }
    };

    return (
        <FormProvider methods={form} onSubmit={handleSubmit(onSubmit)}>
            <Stack direction="row" spacing={2} alignItems='center' justifyContent='flex-end'>
                <RHFTextField size='small' name="username" label='User Name' />
                <RHFTextField type='password' size='small' name="password" label='Password' />
                <LoadingButton
                    type="submit"
                    variant="contained"
                    color="primary"
                    loading={formState.isSubmitting}

                >
                    Login/Signup
                </LoadingButton>
            </Stack>
        </FormProvider>
    );
}