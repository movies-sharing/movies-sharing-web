import reducer from '../redux/slices/movies';
import {Movie} from "../@types/movie";
import {getMovies, shareMovie} from "../redux/slices/movies/movies.thunks";

describe('movies reducer', () => {
    test('should return the initial state', () => {
        expect(reducer(undefined, { type: undefined }).list).toEqual([])
    })

    test('should handle a movies list being added to an empty list', () => {
        const actionPending = getMovies.pending('loading', undefined);
        const mockReducerPending = reducer({ list: [], isLoading: false }, actionPending);
        expect(mockReducerPending.list).toEqual([]);
        expect(mockReducerPending.isLoading).toEqual(true);
        const previousState: Movie[] = [
            {
                id: 1,
                title: 'test 1',
                description: 'description 1',
                sharedBy: 'user 1',
                youtubeUrl: 'https://www.youtube.com/watch?v=1',
                videoId: '1',
                createdAt: new Date(),
            },
            {
                id: 2,
                title: 'test 2',
                description: 'description 2',
                sharedBy: 'user 2',
                youtubeUrl: 'https://www.youtube.com/watch?v=2',
                videoId: '2',
                createdAt: new Date(),
            }
        ]
        const action = getMovies.fulfilled(previousState, 'requestId', undefined);
        const mockReducer = reducer({ list: [], isLoading: false }, action);
        expect(mockReducer.list).toEqual(previousState);
        expect(mockReducer.isLoading).toEqual(false);
    })

    test('should handle a movie being added to an existed list', () => {
        const previousState: Movie[] = [
            {
                id: 1,
                title: 'test 1',
                description: 'description 1',
                sharedBy: 'user 1',
                youtubeUrl: 'https://www.youtube.com/watch?v=1',
                videoId: '1',
                createdAt: new Date(),
            }
        ]
        const actionPending = shareMovie.pending('loading', 'https://www.youtube.com/watch?v=1');
        const mockReducerPending = reducer({ list: previousState, isLoading: false }, actionPending);
        expect(mockReducerPending.list).toEqual(previousState);
        expect(mockReducerPending.isLoading).toEqual(true);
        const sharedMovie: Movie = {
            id: 2,
            title: 'test 2',
            description: 'description 2',
            sharedBy: 'user 2',
            youtubeUrl: 'https://www.youtube.com/watch?v=2',
            videoId: '2',
            createdAt: new Date(),
        }
        const action = shareMovie.fulfilled(sharedMovie,'shareSuccess', 'https://www.youtube.com/watch?v=1');
        const mockReducer = reducer({ list: previousState, isLoading: false }, action);
        expect(mockReducer.list).toEqual([sharedMovie,...previousState]);
        expect(mockReducer.list[0].id).toEqual(2);
        expect(mockReducer.isLoading).toEqual(false);
    })
});