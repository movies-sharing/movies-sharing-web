import Home from "../pages/Home";
import {screen} from "@testing-library/react";
import '@testing-library/jest-dom';
import React from "react";
import {Movie} from "../@types/movie";
import {renderWithProviders} from "../utils/test-utils";
// test Home component render by list movie in redux
describe('Home component', () => {
    test('renders Home with empty movies list', async () => {
        renderWithProviders(<Home />);
        expect(await screen.findByText('No movies shared')).toBeInTheDocument();
    });

    test('renders Home with movies list has 2 items',  async () => {
        const list: Movie[] = [
            {
                id: 1,
                title: 'test 1',
                description: 'description 1',
                sharedBy: 'user 1',
                youtubeUrl: 'https://www.youtube.com/watch?v=1',
                videoId: '1',
                createdAt: new Date(),
            },
            {
                id: 2,
                title: 'test 2',
                description: 'description 2',
                sharedBy: 'user 2',
                youtubeUrl: 'https://www.youtube.com/watch?v=2',
                videoId: '2',
                createdAt: new Date(),
            }
        ];
        renderWithProviders(<Home />, {preloadedState: {movies: { list, isLoading: false } }});
        expect(screen.queryByText('No movies shared')).not.toBeInTheDocument();
        // check Home component render after fetching list movie
        expect(await screen.findByText('test 1')).toBeInTheDocument();
        expect(await screen.findByText('test 2')).toBeInTheDocument();
    });
});



