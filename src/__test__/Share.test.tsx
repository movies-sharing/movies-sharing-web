import Share from "../pages/Share";
import {fireEvent, screen} from "@testing-library/react";
import '@testing-library/jest-dom';
import React from "react";
import {renderWithProviders} from "../utils/test-utils";

// test Share component
describe('Home component', () => {
    test('renders Share with empty url', async () => {
        renderWithProviders(<Share />);
        // click button share
        fireEvent.submit(screen.getByRole("button"));
        // check error text "URL is required" show
        expect(await screen.findByText("URL is required")).toBeInTheDocument();
    });
    test('renders Share with invalid url', async () => {
        renderWithProviders(<Share />);
        // click button share
        fireEvent.input(screen.getByRole("textbox", { name: /url/i }), {
            target: {
                value: "google.com"
            }
        });
        fireEvent.submit(screen.getByRole("button"));
        // check error text "Youtube URL is not valid" show
        expect(await screen.findByText("Youtube URL is not valid")).toBeInTheDocument();
    });
})
