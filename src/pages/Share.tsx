import React from 'react';
import {Box, Card, CardActions, CardContent, CardHeader, Container} from "@mui/material";
import {LoadingButton} from "@mui/lab";
import {useSnackbar} from "notistack";
import * as Yup from 'yup';
import {FormProvider, RHFTextField} from "../components/hook-form";
import {yupResolver} from "@hookform/resolvers/yup";
import {useForm} from "react-hook-form";
import {useDispatch, useSelector} from "../redux/store";
import {shareMovie} from "../redux/slices/movies/movies.thunks";
import {YOUTUBE_URL_REGEX} from "../config";


type FormValuesProps = {
    url: string;
};
export default function Share() {
    const { enqueueSnackbar } = useSnackbar();
    const dispatch = useDispatch();
    const { isLoading } = useSelector((state) => state.movies);


    const ShareSchema = Yup.object().shape({
        url: Yup.string().required('URL is required').trim().matches(YOUTUBE_URL_REGEX, 'Youtube URL is not valid')
    });

    const defaultValues = {
        url: '',
    };

    const form = useForm<FormValuesProps>({
        defaultValues,
        resolver: yupResolver(ShareSchema),
    });

    const { handleSubmit, formState } = form;
    const { errors } = formState;

    const onSubmit = async (data: FormValuesProps) => {
        try {
            const resultAction = await dispatch(shareMovie(data.url));
            if (shareMovie.fulfilled.match(resultAction)) {
                enqueueSnackbar('Share the movie successful', { variant: 'success' });
                form.reset();
            }
            else if (resultAction.payload) {
                enqueueSnackbar(
                    <div>
                        <p>Share video fail with code {resultAction.payload.code}</p>
                        <p>Reason: {resultAction.payload.message}</p>
                    </div>
                    , { variant: 'error' });
            }
            else {
                enqueueSnackbar('Share the movie failed', { variant: 'error' })
            }

        }
        catch (error) {
            enqueueSnackbar('Share the movie failed', { variant: 'error' });
        }
    };


    return (
        <Container maxWidth='xs' disableGutters>
            <Box>
                <FormProvider methods={form} onSubmit={handleSubmit(onSubmit)}>
                    <Card>
                        <CardHeader title='Share a Youtube movie' />
                        <CardContent sx={{ minHeight: '130px'}}>
                            <Box>
                                <RHFTextField fullWidth name="url" label='Youtube URL' helperText={errors.url?.message} />
                            </Box>
                        </CardContent>
                        <CardActions sx={{ display: 'flex', justifyContent: 'center' }}>
                            <LoadingButton
                                type='submit'
                                sx={{ width: '150px'}}
                                variant='contained'
                                color='primary'
                                loading={isLoading}
                            >
                                Share
                            </LoadingButton>
                        </CardActions>
                    </Card>
                </FormProvider>
            </Box>
        </Container>

    );
}