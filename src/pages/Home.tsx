import React, {useEffect} from 'react';
import {
    Box,
    Card,
    CardContent,
    Container,
    Grid,
    Skeleton,
    Typography
} from "@mui/material";
import {useDispatch, useSelector} from "../redux/store";
import InfiniteScroll from "react-infinite-scroll-component";
import {getMovies} from "../redux/slices/movies/movies.thunks";


export default function Home() {
    const dispatch = useDispatch();
    const { list, isLoading } = useSelector((state) => state.movies);

    useEffect(() => {
        dispatch(getMovies());
    }, [dispatch]);

    if (isLoading) {
        return (
            <Container maxWidth={'md'}>
                <Card>
                    <CardContent sx={{ width: '130px'}}>
                        <Grid container spacing={2} alignItems='center'>
                            <Grid item xs={12} md={5}>
                                <Skeleton variant="text" width={150} height={'80px'} />
                            </Grid>
                            <Grid item xs={12} md={7}>
                                <Skeleton variant="text" width={150} height={'15px'} sx={{ mb: 1 }} />
                                <Skeleton variant="text" width={150} height={'15px'} sx={{ mb: 1 }} />
                                {/*<Typography variant='body2' gutterBottom>Shared at: {item.createdAt}</Typography>*/}
                                <Skeleton variant="text" width={150} height={'15px'} sx={{ mb: 1 }} />
                                <Skeleton variant="text" width={150} height={'30px'} sx={{ mb: 1 }} />
                            </Grid>

                        </Grid>
                    </CardContent>
                </Card>
            </Container>
        );
    }

    if (list === undefined || list.length === 0) {
        return (
            <Container maxWidth={'md'}>
                <Box sx={{ display: 'flex', justifyContent: 'center', mx: 4 }}>
                    <Typography variant={'h5'} color='darkgrey' >No movies shared</Typography>
                </Box>
            </Container>
        );
    }

    return (
        <Container maxWidth='lg'>
            <InfiniteScroll
                dataLength={list.length}
                loader={<h4>Loading...</h4>}
                hasMore={false}
                next={() => {}}
            >
                {list.map((item, index) => (
                    <Box key={index} sx={{ px: 4, py: 1 }}>
                        <Card>
                            <CardContent>
                                <Grid container spacing={2} alignItems='center'>
                                    <Grid item xs={12} md={6}>
                                        {/* eslint-disable-next-line jsx-a11y/iframe-has-title */}
                                        <iframe width={"100%"} height="300"
                                                src={`https://www.youtube.com/embed/${item.videoId}`}>
                                        </iframe>
                                    </Grid>
                                    <Grid item xs={12} md={6}>
                                        <Typography variant='h6' gutterBottom>{item.title}</Typography>
                                        <Typography variant='caption' gutterBottom>Shared By: {item.sharedBy}</Typography>
                                        <Typography variant={'body1'} sx={{ mt: 1 }} gutterBottom>Description</Typography>
                                        <Typography variant='subtitle2' color='grey' gutterBottom>{item.description}</Typography>
                                    </Grid>

                                </Grid>
                            </CardContent>
                        </Card>
                    </Box>

                ))
                }

            </InfiniteScroll>
        </Container>
    );
}