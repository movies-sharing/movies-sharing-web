import axios from 'axios';
// config
import {HOST_API} from '../config';

// ----------------------------------------------------------------------

const axiosGatewayInstance = axios.create({
  baseURL: HOST_API,
  headers: {
      'Access-Control-Allow-Credentials':true,
      'Access-Control-Expose-Headers': 'Authorization',
      'Accept': 'application/json',
      'Content-Type': 'application/json',
  },
  responseType: 'json'
});

axiosGatewayInstance.interceptors.response.use(
    (response) => {
        if (response.status === 200) {
            if (response.request.responseURL.includes('/login')) {
                return response;
            }
            return response.data;
        }
        throw response.data

    },
    (error) => {
        if (error.response && error.response.request.responseURL.includes('/login')) {
            throw new Error("User name or password is incorrect!");
        }
        if (error.response && error.response.data) {
            throw error.response.data;
        }
        else {
            throw new Error(error.message);
        }
    }
)

export default axiosGatewayInstance;
