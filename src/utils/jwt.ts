import axiosGatewayInstance from './axios';


const setSession = (accessToken: string | null | undefined, isLogin: boolean, username?: string) => {
  if (accessToken) {
    if (isLogin && username) {
      localStorage.setItem('accessToken', accessToken);
      localStorage.setItem('username', username);
    }
    axiosGatewayInstance.defaults.headers.common.Authorization = `Bearer ${accessToken}`;
  }
  else {
    delete axiosGatewayInstance.defaults.headers.common.Authorization;
  }
};

export { setSession };
