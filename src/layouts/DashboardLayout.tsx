import React from 'react';
import {Outlet, useNavigate} from 'react-router-dom';
// @mui
import {
    AppBar,
    Box, Button,
    Link,
    Stack,
    Toolbar,
    Typography
} from '@mui/material';
//
import Main from './Main';
import LoginForm from "../sections/common/LoginForm";
import useAuthContext from "../hooks/useAuthContext";
import {useSnackbar} from "notistack";
import Iconify from "../components/iconify";

// ----------------------------------------------------------------------

export default function DashboardLayout() {

    const navigate = useNavigate();
    const { enqueueSnackbar } = useSnackbar();
    const { isAuthenticated, username, logout } = useAuthContext();

    const logoutFunc = async () => {
        await logout();
        enqueueSnackbar('Logout successful', { variant: 'success' });
    }

    return (
      <>
          <AppBar position="static" color='default'>
              <Toolbar sx={{ justifyContent: 'space-between' }}>
                  <Link
                      underline="none"
                      component={"button"}
                      onClick={() => navigate('/')}
                  >
                      <Stack direction="row" alignItems="center" spacing={1}>
                          <Iconify icon='material-symbols:home' sx={{ width: 30, height: 30 }} />
                          <Typography variant="h5" color="inherit">Funny Movies</Typography>
                      </Stack>
                  </Link>

                  <Box>
                      {
                          isAuthenticated
                              ? <Stack direction="row" alignItems="center" spacing={1}>
                                    <Typography variant="body1" color="inherit">Welcome, {username}</Typography>
                                    <Button variant="contained" color="primary" onClick={() => navigate('/share')}>Share a movie</Button>
                                    <Button variant="contained" color="secondary" onClick={logoutFunc}>Logout</Button>
                              </Stack>
                              : <LoginForm />
                      }
                  </Box>
              </Toolbar>
          </AppBar>
          <Main>
              <Outlet />
          </Main>
      </>
    );
}
