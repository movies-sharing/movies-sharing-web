// @mui
import { Box, BoxProps } from '@mui/material';

// ----------------------------------------------------------------------

export default function Main({ children }: BoxProps) {
    return (
      <Box
        component="main"
        sx={{
            px: 2,
            pt: `80px`,
            pb: `80px`,
        }}
      >
        {children}
      </Box>
    );
}
