export const HOST_API = `${window ? window.location.protocol : 'http:'}//`
    + `${process.env.REACT_APP_API_SERVER_HOST ?? window.location.hostname}`
    + `:${process.env.REACT_APP_API_SERVER_PORT}`;
export const YOUTUBE_URL_REGEX = /http(?:s)?:\/\/(?:m.)?(?:www\.)?youtu(?:\.be\/|(?:be-nocookie|be)\.com\/(?:watch|[\w]+\?(?:feature=[\w]+.[\w]+\&)?v=|v\/|e\/|embed\/|live\/|user\/(?:[\w#]+\/)+))([^&#?\n]+)/