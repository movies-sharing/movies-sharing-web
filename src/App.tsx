import React from 'react';
import './App.css';
import { Provider as ReduxProvider } from "react-redux";
import { persistor, store } from "./redux/store";
import { BrowserRouter } from "react-router-dom";
import { PersistGate } from 'redux-persist/lib/integration/react';
import SnackbarProvider from "./components/snackbar";
import Router from "./routes";
import {AuthProvider} from "./contexts/JWTContext";
import ThemeProvider from "./theme";

function App() {
  return (
      <AuthProvider>
        <ReduxProvider store={store()}>
          <PersistGate loading={null} persistor={persistor}>
            <BrowserRouter>
                <ThemeProvider>
                    <SnackbarProvider>
                        <Router />
                    </SnackbarProvider>
                </ThemeProvider>
            </BrowserRouter>
          </PersistGate>
        </ReduxProvider>
      </AuthProvider>

  );
}

export default App;
