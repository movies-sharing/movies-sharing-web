FROM node:19-alpine
EXPOSE 80
COPY . /var/www/localhost/htdocs
RUN apk add nginx && \
    cd /var/www/localhost/htdocs && \
    yarn install && \
    yarn build && \
    yarn cache clean && \
    mv /var/www/localhost/htdocs/build /var/www/localhost/movies-sharing-web && \
    cd /var/www/localhost/htdocs && \
    rm -rf * && \
    mv /var/www/localhost/movies-sharing-web /var/www/localhost/htdocs;
CMD ["/bin/sh", "-c", "exec nginx -g 'daemon off;';"]
WORKDIR /var/www/localhost/htdocs
